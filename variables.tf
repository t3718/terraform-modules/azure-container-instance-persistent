variable "prefix" {
  type        = string
  description = "The prefix used for all resources in this module"
  default     = "prefix"
}

variable "environment" {
  type        = string
  description = "The environment name"
  default     = "int"
}

variable "azurerm_resource_group_name" {
  type        = string
  description = "The Azure resource group where all resources in this module should be created"
  default     = "iac-rg"
}

variable "azurerm_resource_group_location" {
  type        = string
  description = "The Azure location where all resources in this module should be created"
  default     = "location"
}

variable "pers_app_name" {
  type        = string
  description = "The application name"
  default     = "aci-example"
}

variable "pers_ip_address_type" {
  type        = string
  description = "ip address type"
  default     = "public"
}

variable "pers_os_type" {
  type        = string
  description = "OS type where to orchestrate the containers"
  default     = "linux"
}

variable "pers_container_name" {
  type        = string
  description = "The docker container name"
  default     = "webtop"
}

variable "pers_container_image" {
  type        = string
  description = "The docker container image"
  default     = "lscr.io/linuxserver/webtop:ubuntu-xfce"
}

variable "pers_container_cpu" {
  type        = string
  description = "The docker container cpu"
  default     = "0.5"
}

variable "pers_container_memory" {
  type        = string
  description = "The docker container memory (RAM)"
  default     = "1.5"
}

variable "pers_container_port" {
  type        = string
  description = "The docker container port"
  default     = "80"
}

variable "pers_container_protocol" {
  type        = string
  description = "The docker container protocol TCP or UDP"
  default     = "TCP"
}

variable "pers_storage_share_name" {
  type        = string
  description = "The Azure storage share name"
  default     = "aci-iac-workflow-share"
}